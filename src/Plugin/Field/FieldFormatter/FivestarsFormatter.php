<?php

namespace Drupal\simple_fivestars\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * @FieldFormatter(
 *   id = "fivestars",
 *   label = @Translation("Fivestars"),
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float",
 *   },
 * )
 */
class FivestarsFormatter extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $item) {
      $elements[] = [
        '#theme' => 'fivestars',
        '#number' => $item->value,
      ];
    }

    return $elements;
  }

}
