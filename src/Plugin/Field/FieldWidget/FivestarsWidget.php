<?php

namespace Drupal\simple_fivestars\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldWidget(
 *   id = "fivestars",
 *   label = @Translation("Fivestars"),
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float",
 *   },
 * )
 */
class FivestarsWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'hide_label' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = parent::settingsForm($form, $form_state);

    $element['hide_label'] = [
      '#type' => 'checkbox',
      '#title' => t('Hide label'),
      '#default_value' => $this->getSetting('hide_label'),
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      '#type' => 'fivestars',
      '#default_value' => $items[$delta]->value,
    ];

    if ($this->getSetting('hide_label')) {
      $element['value']['#title_display'] = 'invisible';
    }

    return $element;
  }

}
