<?php

namespace Drupal\simple_fivestars\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Render\Markup;

/**
 * @FormElement("fivestars")
 */
class FivestarsElement extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#input' => TRUE,
      '#pattern' => '[0-5]',
      '#process' => [
        [$class, 'processFivestars'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Process element.
   */
  public static function processFivestars(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['radios'] = [
      '#prefix' => '<div class="form-fivestars">',
      '#suffix' => '</div>',
      '#markup' => '',
    ];

    $default_value = $element['#default_value'] ?? 0;

    foreach (range(0, 5) as $number) {
      // @TODO Add support #ajax
      $radio_id = $element['#id'] . '-' . $number;
      $element['radios']['#markup'] .= '
        <input type="radio" name="' . $element['#name'] . '" value="' . $number . '"' . ($default_value == $number ? ' checked' : '') . ' id="' . $radio_id . '" />
        <label for="' . $radio_id . '" title="' . $number . '"></label>
      ';
    }

    $element['radios']['#markup'] = Markup::create($element['radios']['#markup']);

    $element['#attached']['library'][] = 'simple_fivestars/main';

    return $element;
  }

}
